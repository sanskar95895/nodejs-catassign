const express = require("express");
const db = require("../connection");
const dbRouter = express.Router();

let insertData = (req, res) => {
  let { name, age, breed } = req.body;
  let sql = "INSERT INTO cats (name,age,breed) VALUES( ? , ? , ? )";
  db.query(sql, [name, age, breed], (err, result) => {
    if (err) throw err;
    res.status(200).send(result).json({
      data: result,
    });
  });
};

let getCats = (req, res) => {
  let sql = "SELECT * FROM cats";
  db.query(sql, (err, result) => {
    if (err) throw err;
    res.status(200).json({
      data: result,
    });
  });
};

let getCat = (req, res) => {
  let sql = `SELECT * from cats where cats_id=${req.params.id}`;
  db.query(sql, (err, result) => {
    if (err) throw err;
    res.status(200).json({
      data: result,
    });
  });
};

let deleteCatData = (req, res) => {
  let sql = `DELETE from cats where cats_id=${req.params.id}`;
  db.query(sql, (err, result) => {
    if (err) throw err;
    res.status(200).json({
      data: result,
    });
  });
};

let updateCatData = (req, res) => {
  let { name, age, breed } = req.body;
  let sql = `UPDATE cats SET name= ?, age= ?, breed = ? where cats_id=${req.params.id}`;
  db.query(sql, [name, age, breed], (err, result) => {
    if (err) throw err;
    res.status(200).json({
      data: result,
    });
  });
};

let givenAgeCats = (req, res) => {
  let sql = `Select * from cats where age between 10 and 20`;
  db.query(sql, (err, result) => {
    if (!err) {
      console.log(result);
      res.status(200).json({
        msg: "getting Data",
        result: result,
      });
    } else throw err;
  });
};

dbRouter.route("/age").get(givenAgeCats);
dbRouter.route("/").post(insertData).get(getCats);
dbRouter.route("/:id").get(getCat).delete(deleteCatData).put(updateCatData);

module.exports = dbRouter;
