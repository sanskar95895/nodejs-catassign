const express = require("express");
const app = express();
const port = process.env.PORT || 3001;

const dbRouter = require("./routers/dbRouter");

app.use(express.json());
app.use("/api/cat", dbRouter);

app.listen(port, (err) => {
    if (err) {
        throw err;
    }
    console.log(`server is successfully started at port ${port}`);
})
